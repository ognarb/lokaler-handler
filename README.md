# NAME TO BE DEFINED

## Idea

Allgemein:
Die gesamte Idee ist, Kunden und Verkäufer auf einer Seite zu vereinen um so ein gegengewicht zum
Internethandel herzustellen und regionale anbieter zu fördern. Hierzu soll die Seite verschiedenen
Nutzergruppen Dienste bereitstellen. Die zwei Hauptgruppen sind Kunden und Verkäufer. Die
Verkäufer lassen sich sicherlich nochmal inn Untergruppen unterteilen. Eine erste Idee dazu wären
beipsielsweise Lebensmittelläden, Frischwarenverkäufer (Bäckerreien, Metzger und Bauern), sowie
Markthändler (aller Art), Elektronikgeschäfte und Warenhäuser für Gebrauchsgüter.

Kunde:
Der Kunde sucht meist nach einem Produkt. Für die Kunden wäre es sinnvoll, wenn es eine Suchzeile
für Produkt gibt und die Möglichkeit eine Postleitzahl anzugeben, damit die Seite weiß, wo man ist
und wo sie suchen soll. Also exakt wie bei Ebay. Vielleicht sollte man bei der Lokalisierung ( um die
Privatsphäre zu waren) je Ort, einen "marktplatz" festlegen, von dem aus gesucht wird, man sagt man
will an Platz X an folgender Postleitzahl suchen und dann wird im Umkreis davon gesucht. Danach
erhält man eine Auflistung aller Ergebnisse, vielleicht nach Abständen gestaffelt, alles im 5km Radius,
alles im 10 km Radius und so weiter. Dann wird das Geschäft mit dem Produkt angezeigt, (mit oder
ohne Preis? - vielleicht nur eine Preiseinordnung? also alle Verkäufer müssen die Preise angeben, aber
nur, damit die Seite einschätzen kann, ob das Produkt teuer-€€€, normal€€, billig-€ ist, oder eben eine
ähnliche Bewertungsfunktion), denn die Seite soll ja nicht dazu gedacht sein, den billigsten Preis zu
finden, sondern nur dazu, Produkte regional zu finden.
Super wäre auch eine Angebotsfunktion, so dass auf der Startseite der Kunden ein Bereich Angebote
auftaucht, wo die Verkäufer Produkte im Angebot zeigen können (kann man dann so durchwischen -
links rechts für gut oder schlecht). Außerdem kann man dieses Projekt „Too good to go“ einbauen, also
ab 18:30 werden hier Produkte eingestellt die am Tag noch übrig sind und verderblich sind .
Jeder Kunde sollte außerdem die Möglichkeit haben Favoriten zu seinen Geschäften hinzuzufügen um
sie schneller zu finden und dort wird auch bevorzugt gesucht,sofern sie nicht gerade zu weit entfernt
sind. Dann wäre natürlich auch eine Bestellfunktion toll, bei der man gerade in Bäckereien
beispielsweise Brötchen vorbestellen kann, damit der Bäcker besser planen kann.

Händler:
Für den Händler gibt es eine „mich anlegen Funktion“ à la Facebook Profil mit Adresse, Webseite,
Telefonnummer,E-Mail etc. damit man den Verkäufer findet und einordnen kann. Außerdem kann er
dann Produktsorten und Produkte anlegen, die aber teilweise schon vordefiniert sind, damit die Suche
erleichtert wird. Nudeln sind eben Nudeln und jedes Produkt hat einen Namen, den kann der Verkäufer
festlegen und dann muss er eine passende Kategorie auswählen oder im schlimmsten Fall anlegen.
Dann wäre es natürlich auch cool, wenn echte Markthändler ihre Produkte tagweise auf dem jeweiligen
Marktplatz anbieten könnten.


## Development

Copy `.env` in `.env.local`. And modify the `DATABASE_URL`.

```
composer install
yarn install
yarn encore dev
bin/console doctrine:database:create
bin/console doctrine:migrations:migrate
bin/console server:start
```

## License

This project is released under AGPL-3.0-or-later
